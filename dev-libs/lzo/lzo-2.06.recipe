SUMMARY="LZO is a portable lossless data compression library written in ANSI C."
DESCRIPTION="
LZO is a data compression library which is suitable for data de-/compression in
real-time. This means it favours speed over compression ratio.

LZO is written in ANSI C. Both the source code and the compressed data format
are designed to be portable across platforms.

LZO implements a number of algorithms with the following features:
* Decompression is simple and *very* fast.
* Requires no memory for decompression.
* Compression is pretty fast.
* Requires 64 kB of memory for compression.
* Allows you to dial up extra compression at a speed cost in the compressor.
	The speed of the decompressor is not reduced.
* Includes compression levels for generating pre-compressed data which achieve
	a quite competitive compression ratio.
* There is also a compression level which needs only 8 kB for compression.
* Algorithm is thread safe.
* Algorithm is lossless.
* LZO supports overlapping compression and in-place decompression.
"
HOMEPAGE="http://www.oberhumer.com/opensource/lzo/"
SRC_URI="http://www.oberhumer.com/opensource/lzo/download/lzo-2.06.tar.gz"
CHECKSUM_SHA256="ff79e6f836d62d3f86ef6ce893ed65d07e638ef4d3cb952963471b4234d43e73"
REVISION="1"
ARCHITECTURES="x86_gcc2 x86"
SECONDARY_ARCHITECTURES="x86"

PROVIDES="
	lzo$secondaryArchSuffix = $portVersion
	lib:liblzo2$secondaryArchSuffix
	"

REQUIRES="
	haiku$secondaryArchSuffix
	"

BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel
	cmd:gcc$secondaryArchSuffix
	cmd:make
	cmd:awk
	"

BUILD()
{
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install
	
	prepareInstalledDevelLib liblzo2
	packageEntries devel $developDir
}

TEST()
{
	make check
}

PROVIDES_devel="
	lzo${secondaryArchSuffix}_devel = $portVersion
	devel:liblzo2$secondaryArchSuffix
	"

REQUIRES_devel="
	lzo$secondaryArchSuffix == $portVersion base
	"

LICENSE="GNU GPL v2"
COPYRIGHT="1996-2011 Markus F.X.J Oberhumer"
