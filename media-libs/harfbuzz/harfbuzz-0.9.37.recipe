SUMMARY="HarfBuzz is an OpenType text shaping engine."
DESCRIPTION="
HarfBuzz is an OpenType text shaping engine. It is used to layout complex text
such as the Indic and Arabic alphabets.
"
HOMEPAGE="http://www.freedesktop.org/wiki/Software/HarfBuzz/"
LICENSE="MIT"
COPYRIGHT="
	2010,2011,2012 Google, Inc.
	2012 Mozilla Foundation
	2011 Codethink Limited
	2008,2010 Nokia Corporation and/or its subsidiary(-ies)
	2009 Keith Stribley
	2009 Martin Hosken and SIL International
	2007 Chris Wilson
	2006 Behdad Esfahbod
	2005 David Turner
	2004,2007,2008,2009,2010 Red Hat, Inc.
	1998-2004 David Turner and Werner Lemberg
	"
SRC_URI="http://www.freedesktop.org/software/harfbuzz/release/harfbuzz-$portVersion.tar.bz2"
CHECKSUM_SHA256="255f3b3842dead16863d1d0c216643d97b80bfa087aaa8fc5926da24ac120207"
REVISION="1"
ARCHITECTURES="x86 x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"

PATCHES="harfbuzz-$portVersion.patchset"

PROVIDES="
	harfbuzz$secondaryArchSuffix = $portVersion compat >= 0.9
	lib:libharfbuzz$secondaryArchSuffix = 0.937.0 compat >= 0.937
	cmd:hb_ot_shape_closure$secondaryArchSuffix
	cmd:hb_shape$secondaryArchSuffix
	"
REQUIRES="
	haiku$secondaryArchSuffix
	lib:libfreetype$secondaryArchSuffix
	lib:libglib_2.0$secondaryArchSuffix
	lib:libiconv$secondaryArchSuffix
	lib:libintl$secondaryArchSuffix
	lib:libpng16$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	lib:libbz2$secondaryArchSuffix
	lib:libstdc++$secondaryArchSuffix
	lib:libgraphite2$secondaryArchSuffix
	"
BUILD_REQUIRES="
	devel:libfreetype$secondaryArchSuffix
	devel:libglib_2.0$secondaryArchSuffix
	devel:libiconv$secondaryArchSuffix
	devel:libintl$secondaryArchSuffix
	devel:libpng16$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	devel:libbz2$secondaryArchSuffix
	devel:libgraphite2$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel
	cmd:automake
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:libtool
	cmd:make
	cmd:grep
	cmd:sed
	cmd:pkg_config$secondaryArchSuffix
"

BUILD()
{
	runConfigure ./configure --with-graphite2=yes
	make $jobArgs
}

INSTALL()
{
	make install

		# remove libtool library file
	rm $libDir/libharfbuzz.la
	
	prepareInstalledDevelLibs libharfbuzz
	fixPkgconfig

	# devel package
	packageEntries devel \
		$developDir \
		$dataDir

}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	harfbuzz${secondaryArchSuffix}_devel = $portVersion compat >= 0.9
	devel:libharfbuzz$secondaryArchSuffix = 0.937.0 compat >= 0.937
	"
REQUIRES_devel="
	harfbuzz$secondaryArchSuffix == $portVersion base
	"
